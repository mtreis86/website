[]{#yandex_top}

::: {#content}
::: {#mw-js-message style="display:none;"}
:::

::: {#mw-body}
[]{#top}

User:DarkladyLexy/Lexys LOTD SE Prerequisites {#firstHeading .firstHeading}
=============================================

::: {#siteSub}
From S.T.E.P. Project Wiki
:::

::: {#mw-content-text .mw-content-ltr dir="ltr" lang="en"}
::: {style="text-align: center;"}
Welcome to Lexy\'s: Legacy of The Dragonborn Special Edition
:::

::: {style="text-align: center;"}
A STEP Hosted guide based of Neovalen\'s Skyrim Revisited: Legendary
Edition
:::

::: {style="text-align: center;"}
Created and maintained by Darth\_mathias and DarkladyLexy With help from
Nozzer66
:::

::: {style="text-align: center;"}
Last Update: 13:15:25 10 October 2018 (GMT)
:::

\

::: {style="text-align: center;"}
[![btn\_donate\_LG.gif](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N5TLCEMXY7KQU){.external
.text}\
[Help support STEP! ALL donations are applied to operating
expenses.]{style="color: #e0dfff"}
:::

\

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------
  [![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/40px-Info-Logo.png){width="40" height="40" srcset="/images/thumb/4/40/Info-Logo.png/60px-Info-Logo.png 1.5x, /images/4/40/Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}   **Notice:** Please note this guide is currently considered a Beta version best suited for Testers and Experienced Modders
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------

    FORUMS: If you want to check Lexy's: Legacy of The Dragonborn Special Edition forums Here
    HISTORY: You can check on revisions of this Prerequisite page by clicking Here
    MAIN GUIDE: Click here to go to the main guide -> Lexy's: LOTD SE

+-----------------------------------------------------------------------+
| +------------------------------------------------------------------+  |
| | ::: {#toctitle}                                                  |  |
| | Contents                                                         |  |
| | --------                                                         |  |
| | :::                                                              |  |
| |                                                                  |  |
| | -   [[1]{.tocnumber} [Official Bethesda                          |  |
| |     Content]{.toctext}](#Official_Bethesda_Content)              |  |
| |     -   [[1.1]{.tocnumber} [Steam                                |  |
| |         Overlay]{.toctext}](#Steam_Overlay)                      |  |
| | -   [[2]{.tocnumber} [Configure                                  |  |
| |     Skyrim]{.toctext}](#Configure_Skyrim)                        |  |
| | -   [[3]{.tocnumber} [Configure The Creation                     |  |
| |     Kit]{.toctext}](#Configure_The_Creation_Kit)                 |  |
| | -   [[4]{.tocnumber} [Script                                     |  |
| |     Extenders]{.toctext}](#Script_Extenders)                     |  |
| |     -   [[4.1]{.tocnumber} [\[Skyrim Script                      |  |
| |         Extender\]]{.toctext}](#.5BSkyrim_Script_Extender.5D)    |  |
| | -   [[5]{.tocnumber} [Tools]{.toctext}](#Tools)                  |  |
| |     -   [[5.1]{.tocnumber} [\[7-Zip\]]{.toctext}](#.5B7-Zip.5D)  |  |
| |     -   [[5.2]{.tocnumber} [\[Champollion a PEX to Papyrus       |  |
| |         decompiler\]]{.toctext}](#.5BChampollion_a_PEX_to_Papyru |  |
| | s_decompiler.5D)                                                 |  |
| |     -   [[5.3]{.tocnumber} [\[LOOT\]]{.toctext}](#.5BLOOT.5D)    |  |
| |     -   [[5.4]{.tocnumber} [LOOT Creating Mod                    |  |
| |         Groups]{.toctext}](#LOOT_Creating_Mod_Groups)            |  |
| |     -   [[5.5]{.tocnumber} [\[Merge Plugins                      |  |
| |         Standalone\]]{.toctext}](#.5BMerge_Plugins_Standalone.5D |  |
| | )                                                                |  |
| |     -   [[5.6]{.tocnumber} [\[Mator                              |  |
| |         Smash\]]{.toctext}](#.5BMator_Smash.5D)                  |  |
| |     -   [[5.7]{.tocnumber}                                       |  |
| |         [\[Relinker\]]{.toctext}](#.5BRelinker.5D)               |  |
| |     -   [[5.8]{.tocnumber}                                       |  |
| |         [\[SSEEDIT\]]{.toctext}](#.5BSSEEDIT.5D)                 |  |
| |     -   [[5.9]{.tocnumber} [\[SSE Nif                            |  |
| |         Optimizer\]]{.toctext}](#.5BSSE_Nif_Optimizer.5D)        |  |
| |     -   [[5.10]{.tocnumber} [\[Wrye                              |  |
| |         Bash\]]{.toctext}](#.5BWrye_Bash.5D)                     |  |
| |     -   [[5.11]{.tocnumber} [\[Zedit\]]{.toctext}](#.5BZedit.5D) |  |
| | -   [[6]{.tocnumber} [xEdit Scripts]{.toctext}](#xEdit_Scripts)  |  |
| |     -   [[6.1]{.tocnumber} [\[MXPF - Mator\'s xEdit Patching     |  |
| |         Framework\]]{.toctext}](#.5BMXPF_-_Mator.27s_xEdit_Patch |  |
| | ing_Framework.5D)                                                |  |
| | -   [[7]{.tocnumber} [zEdit Modules]{.toctext}](#zEdit_Modules)  |  |
| |     -   [[7.1]{.tocnumber} [\[UPF No Dragon LODs                 |  |
| |         Patcher\]]{.toctext}](#.5BUPF_No_Dragon_LODs_Patcher.5D) |  |
| |     -   [[7.2]{.tocnumber}                                       |  |
| |         [\[hishy-npc-enchant-fix\]]{.toctext}](#.5Bhishy-npc-enc |  |
| | hant-fix.5D)                                                     |  |
| |     -   [[7.3]{.tocnumber} [\[UPF Opposite Animation             |  |
| |         Disabler\]]{.toctext}](#.5BUPF_Opposite_Animation_Disabl |  |
| | er.5D)                                                           |  |
| |     -   [[7.4]{.tocnumber} [\[(SJG) UPF Patcher - See the        |  |
| |         Encounter Zone level in the name of the                  |  |
| |         zone\]]{.toctext}](#.5B.28SJG.29_UPF_Patcher_-_See_the_E |  |
| | ncounter_Zone_level_in_the_name_of_the_zone.5D)                  |  |
| |     -   [[7.5]{.tocnumber} [\[UPF Khajiit Ears Show              |  |
| |         Patcher\]]{.toctext}](#.5BUPF_Khajiit_Ears_Show_Patcher. |  |
| | 5D)                                                              |  |
| | -   [[8]{.tocnumber} [Microsoft Visual C++ Redistributable for   |  |
| |     Visual Studio                                                |  |
| |     2017]{.toctext}](#Microsoft_Visual_C.2B.2B_Redistributable_f |  |
| | or_Visual_Studio_2017)                                           |  |
| |     -   [[8.1]{.tocnumber} [\[Microsoft Visual C++               |  |
| |         Redistributable for Visual Studio                        |  |
| |         2017\]]{.toctext}](#.5BMicrosoft_Visual_C.2B.2B_Redistri |  |
| | butable_for_Visual_Studio_2017.5D)                               |  |
| | -   [[9]{.tocnumber} [Mod Manager]{.toctext}](#Mod_Manager)      |  |
| |     -   [[9.1]{.tocnumber} [\[Mod Organizer                      |  |
| |         2\]]{.toctext}](#.5BMod_Organizer_2.5D)                  |  |
| |     -   [[9.2]{.tocnumber} [Installing Mod Organizer             |  |
| |         2]{.toctext}](#Installing_Mod_Organizer_2)               |  |
| |     -   [[9.3]{.tocnumber} [Configuring Mod Organizer            |  |
| |         2]{.toctext}](#Configuring_Mod_Organizer_2)              |  |
| |     -   [[9.4]{.tocnumber} [Mod Organizer 2: Base Profile        |  |
| |         Creation]{.toctext}](#Mod_Organizer_2:_Base_Profile_Crea |  |
| | tion)                                                            |  |
| |     -   [[9.5]{.tocnumber} [Setting up Base Tools for Mod        |  |
| |         Organizer                                                |  |
| |         2]{.toctext}](#Setting_up_Base_Tools_for_Mod_Organizer_2 |  |
| | )                                                                |  |
| |     -   [[9.6]{.tocnumber} [Creating Legacy Of The Dragonborn:   |  |
| |         Special Edition                                          |  |
| |         Profile]{.toctext}](#Creating_Legacy_Of_The_Dragonborn:_ |  |
| | Special_Edition_Profile)                                         |  |
| | -   [[10]{.tocnumber} [Clean the Bethesda                        |  |
| |     ESM\'s]{.toctext}](#Clean_the_Bethesda_ESM.27s)              |  |
| | -   [[11]{.tocnumber} [BethINI]{.toctext}](#BethINI)             |  |
| |     -   [[11.1]{.tocnumber}                                      |  |
| |         [\[BethINI\]]{.toctext}](#.5BBethINI.5D)                 |  |
| | -   [[12]{.tocnumber} [Post BethINI                              |  |
| |     Tweaks]{.toctext}](#Post_BethINI_Tweaks)                     |  |
| |     -   [[12.1]{.tocnumber} [SKSE.ini]{.toctext}](#SKSE.ini)     |  |
| |     -   [[12.2]{.tocnumber} [Skyrim.ini]{.toctext}](#Skyrim.ini) |  |
| |     -   [[12.3]{.tocnumber} [SkyrimPrefs.ini                     |  |
| |         ]{.toctext}](#SkyrimPrefs.ini)                           |  |
| | -   [[13]{.tocnumber} [xLODGEN]{.toctext}](#xLODGEN)             |  |
| |     -   [[13.1]{.tocnumber}                                      |  |
| |         [\[xLODGEN\]]{.toctext}](#.5BxLODGEN.5D)                 |  |
| | -   [[14]{.tocnumber} [DynDOLOD]{.toctext}](#DynDOLOD)           |  |
| |     -   [[14.1]{.tocnumber} [\[DynDOLOD                          |  |
| |         Standalone\]]{.toctext}](#.5BDynDOLOD_Standalone.5D)     |  |
| | -   [[15]{.tocnumber} [Merge Plugins Standalone - Lexy\'s Legacy |  |
| |     of The Dragonborn Special Edition                            |  |
| |     Profile]{.toctext}](#Merge_Plugins_Standalone_-_Lexy.27s_Leg |  |
| | acy_of_The_Dragonborn_Special_Edition_Profile)                   |  |
| | -   [[16]{.tocnumber} [ENB                                       |  |
| |     Installation]{.toctext}](#ENB_Installation)                  |  |
| |     -   [[16.1]{.tocnumber} [\[ENB                               |  |
| |         Series\]]{.toctext}](#.5BENB_Series.5D)                  |  |
| | -   [[17]{.tocnumber} [GPU Driver                                |  |
| |     Settings]{.toctext}](#GPU_Driver_Settings)                   |  |
| |     -   [[17.1]{.tocnumber} [\[NVIDIA Profile                    |  |
| |         Inspector\]]{.toctext}](#.5BNVIDIA_Profile_Inspector.5D) |  |
| +------------------------------------------------------------------+  |
+-----------------------------------------------------------------------+

::: {style="text-align: center;"}
Prerequisites Page
:::

\

[ Official Bethesda Content ]{#Official_Bethesda_Content .mw-headline}
----------------------------------------------------------------------

-   [Skyrim Special
    Edition](http://store.steampowered.com/agecheck/app/489830/){.external
    .text} v1.5.50
-   [Creation Kit](https://bethesda.net/en/dashboard){.external .text}
    v2.0
-   [Dawnguard
    DLC](http://store.steampowered.com/agecheck/app/489830/){.external
    .text}
-   [Hearthfire
    DLC](http://store.steampowered.com/agecheck/app/489830/){.external
    .text}
-   [Dragonborn
    DLC](http://store.steampowered.com/agecheck/app/489830/){.external
    .text}

### [ Steam Overlay ]{#Steam_Overlay .mw-headline}

The Steam Overlay is known to cause issues while playing Skyrim Special
Edition it is advised to disable it; to do this perform the following:

1.  Load [Steam.]{style="color:Gold"}
2.  In the **Tool bar** at the top of the left hand side **Click**
    [Steam.]{style="color:Gold"}
3.  In the Dropdown menu **Click** [Settings.]{style="color:Gold"}
4.  Then in the popup box find where it says
    [In-Game.]{style="color:Gold"}
5.  Uncheck [Enabled the Steam overlay while
    in-game]{style="color:Gold"} then click [OK.]{style="color:Gold"}

Alternatively if you would just like to disable this for Skyrim Special
Edition you may:

1.  Load [Steam.]{style="color:Gold"}
2.  **Click** [LIBRARY.]{style="color:Gold"}
3.  On your **Games** list look for [The Elder Scrolls V: Skyrim Special
    Editon]{style="color:Gold"} and **Right Click**.
4.  In the Dropdown menu **Click** [Properties]{style="color:Gold"}.
5.  Then in the popup box **Uncheck** [Enable the Steam Overlay while
    in-game]{style="color:Gold"}.

[ Configure Skyrim ]{#Configure_Skyrim .mw-headline}
----------------------------------------------------

Before we can actually start doing any modding, we need to setup Skyrim
Special Edition first so we have a starting base to work from.

Start by opening Steam: then perform the following steps in order:

Start The Elder Scrolls V: Skyrim Special Edition through the Steam
context menu to open the default Skyrim Special Edition launcher.

1.  Click [\"Options\"]{style="color:Gold"}.
2.  Click the [\[Ultra\]]{style="color:Gold"} push-button.
3.  Set [\"Aspect Ratio\"]{style="color:Gold"} and [\"Resolution\"
    ]{style="color:Gold"}through the drop down box. Set these to the
    monitors optimal resolution.
4.  Set [\"Antialiasing\"]{style="color:Gold"} to Off through the drop
    down box. This is in preparation for the ENB Install.
5.  Set [\"Full Screen Mode\"]{style="color:Gold"} by making sure
    [Windowed Mode]{style="color:Gold"} is
    [Unticked]{style="color:Gold"}.
6.  Click the [\[OK\]]{style="color:Gold"} button to close and save.

We have just set Skyrim Special Edition up to a base to work on. We\'ll
tweak this when we run the [BethINI ]{style="color:Gold"}tool later in
the guide.

[ Configure The Creation Kit ]{#Configure_The_Creation_Kit .mw-headline}
------------------------------------------------------------------------

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[If
you use Windows Defender Firewall and find yourself crashing while
trying to launch the Creation Kit for the very first time. Try adding an
outbound rule in Windows Defender Firewall for Creation
Kit]{style="margin-left: 1em;color:#cacaca;"}
:::

[Creation Kit SE Tutorial (Installation) by
Darkfox127](https://www.youtube.com/watch?v=J-Sea_pAxlo){.external
.text}

The Creation Kit is a valuable tool for Skyrim Special Edition as it
allows users to create and/or edit mods. It can be used to fix and
change things not possible with the other 3rd party tools. Similar to
Skyrim Special Edition, the Creation Kit does not have default ini files
until it is started for the first time and requires some basic setup to
work properly for the purpose of this guide.

For the puroses of this guide the Creation kit will be required for
proper mod conversion.

1.  Download and install [Bethesda.net
    Launcher](http://download.cdp.bethesda.net/BethesdaNetLauncher_Setup.exe){.external
    .text}.
2.  Run the Launcher and sign in (create an account if you don\'t have
    one).
3.  Users should see the grey \"Creation\" icon on the left. Click on it
    and install.
4.  Users should now [launch]{style="color:Gold"} the CK, choosing
    [YES]{style="color:Gold"} when asked to extract the scripts, and
    close the program.

There are a few edits to the INI file that need to be made before you
can use the CK to port mods.

1.  Download the premade [CreationKitCustom.ini]{style="color:Gold"}
    [here](https://www.dropbox.com/s/xpg5u3s1n7zwjv5/CreationKitCustom.7z?dl=0){.external
    .text}
2.  Extract it to [Steam\\steamapps\\common\\Skyrim Special
    Edition]{style="color:Gold"}

These settings will remain set for each subsequent run. These settings
have accomplished the following:

-   Setup the ini files for multiple master loads and to properly load
    DLC content.

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[The
Scripts archive in the DATA folder can remain after the
extraction.]{style="margin-left: 1em;color:#cacaca;"}
:::

[ Script Extenders ]{#Script_Extenders .mw-headline}
----------------------------------------------------

#### [[\[Skyrim Script Extender\]]{style="color:cyan"}]{#.5BSkyrim_Script_Extender.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Skyrim Script Extender](http://skse.silverlock.org/){.external .text} -
v2.0.8 - by ianpatt, behippo, scruggsywuggsy the ferret, and purple
lunchbox

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Current SE build 2.0.8
    (runtime 1.5.50): 7z archive*
:::
:::

Install only these files to the main *Skyrim Special Edition* folder at
this time:

-   skse64\_1\_5\_50.dll
-   skse64\_loader.exe
-   skse64\_steam\_loader.dll

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[Do
NOT install the script files yet. These will be installed later as part
of the [*Create The Lexy\'s LOTD SE
Profile*](#Create_The_Lexy.27s_LOTD_SE_Profile) section. Do not delete
the downloaded archive.]{style="margin-left: 1em;color:#cacaca;"}
:::

SKSE is absolutely essential. It extends the scripting capacity of the
game to allow mods to do things that would not normally be doable in the
base Skyrim Engine. Many Mods depend upon it.

[ Tools ]{#Tools .mw-headline}
------------------------------

#### [[\[7-Zip\]]{style="color:cyan"}]{#.5B7-Zip.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[7-Zip](http://www.7-zip.org/download.html){.external .text} - v18.05

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *7-Zip for 64-bit Windows
    x64 (Intel 64 or AMD64)*
:::
:::

#### [[\[Champollion a PEX to Papyrus decompiler\]]{style="color:cyan"}]{#.5BChampollion_a_PEX_to_Papyrus_decompiler.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Champollion a PEX to Papyrus
decompiler](https://www.nexusmods.com/skyrim/mods/35307){.external
.text} - 1.01 - by li1nx

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Champollion V1\_0\_1 64
    bits*
:::
:::

Special Installation: [Manually extract the mod and place in the main
Skyrim Special Edition Directory Folder]{style="color:Magenta"}

Mod notes: The function of this tool is to decompile pex scripts to
source code. This is a prerequisite for Merge Plugins Standalone.

#### [[\[LOOT\]]{style="color:cyan"}]{#.5BLOOT.5D .mw-headline}

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[Is
is advised to run LOOT Once Outside of Mod Organizer 2 and sort your
Load Order this will make sure the Masterlist updates
properly]{style="margin-left: 1em;color:#cacaca;"}
:::

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[LOOT](https://github.com/loot/loot/releases/tag/0.13.4){.external
.text} - 0.13.4 - by WrinklyNinja et al

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *LOOT.Installer.exe*
:::
:::

Mod notes: The programs allow you to sort and optimize your Load Order.

Special Installation: [Once LOOT is installed we need to set up some Mod
Groups that will be used throughout this guide these groups are created
by:]{style="color:Magenta"}

#### [LOOT Creating Mod Groups]{#LOOT_Creating_Mod_Groups .mw-headline}

1.  Run LOOT
2.  Navigate to the 3 dots in the far right of the interface.
3.  Select Open Group Editor.
4.  In the Add a new group box Add "Merge Patches"
5.  Left-click the node and Drag the line with the arrow and link
    "Realistic Water" to "Merge Patches" (Merge Patches should turn
    green)
6.  In the Add a new group box Add "NPC Retextures"
7.  Left-click the node and Drag the line with the arrow and link "Merge
    Patches" to "NPC Retextures" (NPC Retextures should turn green)
8.  In the Add a new group box Add "Conflict Resolution"
9.  Left-click the node and Drag the line with the arrow and link "NPC
    Retextures" to \"Conflict Resolution\" (Conflict Resolution should
    turn green)
10. In the Add a new group box Add "Scarcity".
11. Left-click the node and Drag the line with the arrow and link
    "Conflict Resolution" to \"Scarcity\" (Scarcity should turn green)
12. Now Left click the node and Drag the line with the arrow and link
    "Scarcity" to Dynamic Patches (Scarcity should turn grey)
13. In the Add a new group box Add "Arthmoor\'s Villages".
14. Left click on the \"Add-ons & Expansions\" node and Drag the line
    with the arrow and link "Add-ons & Expansions" to \"Arthmoor\'s
    Villages\" (Arthmoor\'s Villages should turn green)
15. Now Left click the \"Arthmoor\'s Villages\" node and Drag the line
    with the arrow and link " Arthmoor\'s Villages\" to \"High Priority
    Overrides\" (Arthmoor\'s Villages should turn grey)

By the end, all the newly Added Groups should be Grey.

Watch [THIS](https://streamable.com/dy9eq){.external .text} handy Video
to see how to create the Mod Groups.

Click [HERE](https://imgur.com/gYcJexm){.external .text} to see it all
finished.

#### [[\[Merge Plugins Standalone\]]{style="color:cyan"}]{#.5BMerge_Plugins_Standalone.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Merge Plugins
Standalone](https://www.nexusmods.com/skyrim/mods/69905/?){.external
.text} - 2.3.1 - by Mator

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Merge Plugins*
:::
:::

Mod notes: The programs allows you to Combine plugin as so reduce
overall esp count.

#### [[\[Mator Smash\]]{style="color:cyan"}]{#.5BMator_Smash.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Mator Smash](https://www.nexusmods.com/skyrim/mods/90987){.external
.text} - 1.0.2 - by Mator

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Mator Smash Hotfix 2*
:::
:::

Mod notes: Mator Smash is a tool that generates conflict resolution
patches, similar to Wrye Bash\'s Bashed Patch.

#### [[\[Relinker\]]{style="color:cyan"}]{#.5BRelinker.5D .mw-headline}

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[Some
people are reporting issues with version 44 of Relinker as it\'s causing
CTD\'s on startup. If this occurs for you use the older version
40.]{style="margin-left: 1em;color:#cacaca;"}
:::

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Relinker](https://www.nexusmods.com/skyrim/mods/77959/?){.external
.text} - v40 - by GandaG

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**OLDER FILES**]{style="color: #d0eb50"} - *Relinker\_64bit*
:::
:::

Mod notes: A small application to assist Merge Plugins Standalone with a
few special functions in scripts and LODGen files.

#### [[\[SSEEDIT\]]{style="color:cyan"}]{#.5BSSEEDIT.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[SSEEDIT](https://github.com/TES5Edit/TES5Edit/releases/tag/xedit-3.2.2){.external
.text} - v3.2.2 - by ElminsterAU and SSEEdit team

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *SSEEdit\_3\_2\_2.zip*
:::
:::

Mod notes: This program is an essential Tool that will help with
cleaning mods of dirty edit and enable to creation of patches required
by this guide.

#### [[\[SSE Nif Optimizer\]]{style="color:cyan"}]{#.5BSSE_Nif_Optimizer.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[SSE NIF
Optimizer](https://www.nexusmods.com/skyrimspecialedition/mods/4089/){.external
.text} - v3.0.6 - by ousnius

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *SSE Nif Optimizer*
:::
:::

Mod notes: This program is Tool that will help optimize Nifs throughout
this guide.

#### [[\[Wrye Bash\]]{style="color:cyan"}]{#.5BWrye_Bash.5D .mw-headline}

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[**If
the Version from Nexus gives you trouble you can find the latest
Development build
[Here](https://afkmods.iguanadons.net/index.php?/topic/4966-wrye-bash-all-games/){.external
.text}, the link *307 WB wip standalone* at the bottom of the second
post *Bug tracking and progress towards
307*.**]{style="margin-left: 1em;color:#cacaca;"}
:::

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Wrye
Bash](https://www.nexusmods.com/skyrimspecialedition/mods/6837?){.external
.text} - Wrye Bash 307.beta3 - by Wrye Bash Team

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Wrye Bash 307 beta3 -
    Standalone Executable*
:::
:::

Special Installation: [Manually extract and install to the main Skyrim
Special Edition directory. Windows 7 users create a Docs Folder in
Skyrim Special Edition\\Data]{style="color:Magenta"}.

Mod notes: The function of this tool is to manage mods and create Bashed
Patches. Bashed Patches merge the levelled lists from the installed mods
and has some limited functionality to merge mods and tweak game values.
For the purposes of this guide, only the Bashed Patch functionality will
be used.

#### [[\[Zedit\]]{style="color:cyan"}]{#.5BZedit.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Zedit](https://github.com/z-edit/zedit/releases/tag/0.4.3){.external
.text} - Alpha Release v0.4.3 - by Mator

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} -
    *zEdit\_Alpha\_v0.4.3\_-\_Portable\_x64.7z*
:::
:::

Special Installation: [Make sure you download and install
zEdit\_Alpha\_v0.4.3\_-\_Portable\_x64.7z]{style="color:Magenta"}.

Mod Notes: This tool is similar to SSEEdit.

[xEdit Scripts]{#xEdit_Scripts .mw-headline}
--------------------------------------------

#### [[\[MXPF - Mator\'s xEdit Patching Framework\]]{style="color:cyan"}]{#.5BMXPF_-_Mator.27s_xEdit_Patching_Framework.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[MXPF - Mator\'s xEdit Patching
Framework](http://www.nexusmods.com/skyrim/mods/68617/?){.external
.text} - 1.1.0 - by Mator

+-----------------------------------------------------------------------+
| **Files to download** :\                                              |
|                                                                       |
| <div>                                                                 |
|                                                                       |
| -   **[MAIN FILES]{style="color: #d0eb50"}** - *MXPF-a*               |
|                                                                       |
| </div>                                                                |
+-----------------------------------------------------------------------+
:::

Mod notes: This mod add a framework to xEdit that other scripts can use.

Special installation: [Download MANUALLY and install the Edit scripts
folder into SSEEdit base folder.]{style="color:gold"}

[zEdit Modules]{#zEdit_Modules .mw-headline}
--------------------------------------------

#### [ [\[UPF No Dragon LODs Patcher\]]{style="color:Cyan"} ]{#.5BUPF_No_Dragon_LODs_Patcher.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[UPF No Dragon LODs
Patcher](https://www.nexusmods.com/skyrimspecialedition/mods/13541){.external
.text} - V1.0 - by Hishy

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *hishy-no-dragon-lods*
:::
:::

Special Installation: [Extract hishy-no-dragon-lods into the \'modules\'
subdirectory of zEdit.]{style="color:Magenta"}

Mod Notes: This mod applies the No Dragon LODs effect.

#### [ [\[hishy-npc-enchant-fix\]]{style="color:Cyan"} ]{#.5Bhishy-npc-enchant-fix.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[hishy-npc-enchant-fix](https://www.nexusmods.com/skyrimspecialedition/mods/13543){.external
.text} - V1.2 - by Hishy and Mator

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *hishyNpcEnchantFix-v1.2*
:::
:::

Special Installation: [Extract hishyNpcEnchantFix into the \'modules\'
subdirectory of zEdit.]{style="color:Magenta"}

Mod Notes: This mod is a simple port of ASIS\' NPC Enchant Fix that will
work with zEdit.

#### [ [\[UPF Opposite Animation Disabler\]]{style="color:Cyan"} ]{#.5BUPF_Opposite_Animation_Disabler.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[UPF Opposite Animation
Disabler](https://www.nexusmods.com/skyrimspecialedition/mods/18281){.external
.text} - V1.1.1 - by Qudix

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} -
    *oppositeAnimationDisabler*
:::
:::

Special Installation: [Extract oppositeAnimationDisabler into the
\'modules\' subdirectory of zEdit.]{style="color:Magenta"}

Mod Notes: A Unified Patching Framework (UPF) patcher that disables the
\'Opposite Gender Anim\' flag on all affected NPCs.

#### [ [\[(SJG) UPF Patcher - See the Encounter Zone level in the name of the zone\]]{style="color:Cyan"} ]{#.5B.28SJG.29_UPF_Patcher_-_See_the_Encounter_Zone_level_in_the_name_of_the_zone.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[(SJG) UPF Patcher - See the Encounter Zone level in the name of the
zone](https://www.nexusmods.com/skyrimspecialedition/mods/19881){.external
.text} - v2.0 - by hishutup and mickthompson4000

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *(SJG) UPF Cell Encounter
    Level in Name Patcher*
:::
:::

Special Installation: [Extract hishy-cell-encounter-level-in-name into
the \'modules\' subdirectory of zEdit.]{style="color:Magenta"}

Mod Notes: This mod adds the Encounter Zone\'s level to the text you see
before you enter the zone.

#### [ [\[UPF Khajiit Ears Show Patcher\]]{style="color:Cyan"} ]{#.5BUPF_Khajiit_Ears_Show_Patcher.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[UPF Khajiit Ears Show
Patcher](https://www.nexusmods.com/skyrimspecialedition/mods/13544){.external
.text} - V1.0 - by Hishutup

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *hishy-khajiit-ears-show*
:::
:::

Special Installation: [Extract hishy-khajiit-ears-show into the
\'modules\' subdirectory of zEdit.]{style="color:Magenta"}

Mod Notes: This mod will allow all Khajiit to show their ears when
wearing helmets.

[Microsoft Visual C++ Redistributable for Visual Studio 2017]{#Microsoft_Visual_C.2B.2B_Redistributable_for_Visual_Studio_2017 .mw-headline}
--------------------------------------------------------------------------------------------------------------------------------------------

#### [[\[Microsoft Visual C++ Redistributable for Visual Studio 2017\]]{style="color:cyan"}]{#.5BMicrosoft_Visual_C.2B.2B_Redistributable_for_Visual_Studio_2017.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Microsoft Visual C++ Redistributable for Visual Studio
2017](https://www.visualstudio.com/downloads/){.external .text} - v
32-bit and 64-bit - by Microsoft

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**Other Tools and Frameworks**]{style="color: #d0eb50"} -
    *Microsoft Visual C++ Redistributable for Visual Studio 2017 x64*
-   [**Other Tools and Frameworks**]{style="color: #d0eb50"} -
    *Microsoft Visual C++ Redistributable for Visual Studio 2017 x86*
:::
:::

Mod Notes: this Tool is required for Mod Organizer 2 to function
properly

[Mod Manager]{#Mod_Manager .mw-headline}
----------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/40px-Info-Logo.png){width="40" height="40" srcset="/images/thumb/4/40/Info-Logo.png/60px-Info-Logo.png 1.5x, /images/4/40/Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}   **Notice:** The paid for version of [ccleaner](https://www.ccleaner.com/){.external .text} is known to break Mod Organizer 2\'s [Virtual Filing System (VFS)](http://wiki.step-project.com/Guide:Mod_Organizer_Advanced#The_virtual_filing_system/){.external .text}. Unchecking the \'windows event logs\' option under ccleaner\'s advanced tab will help ensure this doesn\'t happen. Also make sure the Profile Folder Remains in the same location of Mod Organizer 2 as all the base files otherwise Relinker could break
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### [[\[Mod Organizer 2\]]{style="color:cyan"}]{#.5BMod_Organizer_2.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[Mod Organizer
2](https://www.nexusmods.com/skyrimspecialedition/mods/6194/){.external
.text} - v2.1.5 - by Tannin

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *Mod Organizer 2
    (Archive)*
:::
:::

Mod notes: This is the tool we\'ll actually be using to download,
install and manage the mods. Don\'t worry if it looks daunting, we\'ll
set you right shortly.

### [Installing Mod Organizer 2]{#Installing_Mod_Organizer_2 .mw-headline}

Once Mod Organizer 2 is downloaded some manual setup is required suggest
watching [this](https://www.youtube.com/watch?v=3CE6xpIp3Ys){.external
.text} to familiarize yourself installation procedure (Skip to 10:27):

1.  Create a new folder called [Mod Organizer 2 - Lexys LOTD
    SE]{style="color:Gold"} (or something you would Recognize easily) in
    the [**Root**]{style="color:Gold"} of a Drive (so you have some like
    [E:\\Mod Organizer 2 - Lexys LOTD SE]{style="color:Gold"}).
2.  Extract the contents of the downloaded Zip file to the folder
    created in Step 1
3.  Run ModOrganizer.exe
4.  A popup box should appear, asking you to \"choose an Instance\" pick
    **Portable**
5.  A 2nd popup should appear; from the drop-down menu pick **Skyrim
    Special Edition**
6.  A 3rd popup should appear asking if you want to see the tutorial.
    Those who are new to Mod Organizer 2 should select **Yes**. Those
    who are very familiar with it should Select **No**.
7.  A 4th popup should appear stating that \'Mod Organizer is not set up
    to handle nxm links. Associate it with nxm links?\' The recommended
    response is **Yes**, as this will allow Mod Organizer 2 to handle
    Nexus file links.

Congratulations Mod Organizer 2 is installed. Now we need to configure
it properly.

[Idiot check make sure you don\'t move your mods folder out of Mod
Organizer 2 base folder]{style="color:Orange"}

### [ Configuring Mod Organizer 2 ]{#Configuring_Mod_Organizer_2 .mw-headline}

Now that Mod Organizer 2 is installed to setup up correctly

-   From the toolbar click the Spanner and screwdriver icon.
-   Under the Nexus tab make sure the **Automatically login to Nexus**
    is **Ticked**
-   Input your Nexus User Name and Password.
-   Click OK

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[If
you\'re installing onto a smaller SSD for example and are short for
space consider changing your Downloads location to a larger hard drive.
To do this, make a folder on the hard drive named [\'Skyrim Mod
Downloads\']{style="color:Gold"} or similar. Then on the
[\[Path\]]{style="color:Gold"} tab of MO2 click the box labelled
[\"Downloads\"]{style="color:Gold"} and point the Downloads directory
option to the folder you just made. I suggest not changing any of the
other \"Paths\".]{style="margin-left: 1em;color:#cacaca;"}
:::

Congratulations Mod Organizer 2 should be properly configured. Next, we
need to set up a base profile.

### [ Mod Organizer 2: Base Profile Creation ]{#Mod_Organizer_2:_Base_Profile_Creation .mw-headline}

Now the Mod Organizer 2 is installed and configured correctly, we need
to create a Base Profile to work from.

1.  Click the [\[Configure Profiles\]]{style="color:Gold"} button at the
    top of the window. The icon looks like an id card.
2.  Click [\[Create\] and type [\"Vanilla Skyrim SE\"
    ]{style="color:Gold"}in the text box. ]{style="color:Gold"}
3.  Verify that [\"Default Game Settings\" ]{style="color:Gold"}is
    unchecked then click \[OK\]. The [\"Vanilla Skyrim
    SE\"]{style="color:Gold"} profile will appear in the list.
4.  Exit from the Configure Profiles window and change to the newly
    create Profile.
5.  reopen Configure Profiles window and the Select the [\"Default\"
    ]{style="color:Gold"}profile and click
    [\[Remove\]]{style="color:Gold"}. Select [\[Yes\]
    ]{style="color:Gold"}when prompted.
6.  Close the dialogue by clicking [\[Close\]]{style="color:Gold"}.
7.  Verify that [\"Vanilla Skyrim SE\" ]{style="color:Gold"}is the
    currently selected profile in the \"Profile\" drop-down above the
    left-pane of the Mod Organizer window.

### [ Setting up Base Tools for Mod Organizer 2 ]{#Setting_up_Base_Tools_for_Mod_Organizer_2 .mw-headline}

Now to setup the tool\'s UI and other such settings. Follow the steps
below

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[Some
extenders and tools such as SKSE, Skyrim, Skyrim Launcher, LOOT and the
Creation Kit should appear already configured in the list that will pop
up. To configure the remaining tools, perform the
following:]{style="margin-left: 1em;color:#cacaca;"}
:::

1.  Click the [\[Executables\]]{style="color:Gold"} button at the top of
    the main Mod Organizer window. The icon looks like a pair of cogs.
2.  In the [\"Title\"]{style="color:Gold"} text box type \"SSEEdit\".
3.  In the [\"Binary\"]{style="color:Gold"} text box type [\<SSEEdit
    Path\>\\SSEEdit.exe]{style="color:Gold"} or select it using windows
    explorer by pressing the \[\...\] button.
4.  Do not select any of the optional check boxes.
5.  Click the [\[Add\]]{style="color:Gold"} button. SSEEdit should
    appear in the list.
6.  Repeat this process for [Merge Plugins]{style="color:Gold"}, [Wrye
    Bash]{style="color:Gold"} and [zEdit]{style="color:Gold"}, replacing
    the appropriate fields then close the dialogue by clicking
    \[Close\].

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[What
this step has done is added the tools you\'ll use as part of the modding
process as executables that can be run through Mod Organizer. Most of
the tools we\'ll use will need to be run through Mod Organizer to work
correctly. This is so the tools can \'see\' the virtual file system Mod
Organizer sets up.]{style="margin-left: 1em;color:#cacaca;"}
:::

Next we\'ll fix the default left pane order, which is scrambled from how
it should look. Drag and drop the files in the left pane until they look
like this:

     * Dawnguard
     * HearthFires
     * Dragonborn

We now need to run LOOT to organize the right pane so it depicts the
correct order also. Perform the following steps:

1.  Select [LOOT ]{style="color:Gold"}from the drop-down list of
    executables.
2.  Click the [\[Run\]]{style="color:Gold"} button
3.  Towards the top right-hand corner is an icon that looks like 3
    horizontal lines. Mousing over it shows it\'s for \'Sort Plugins\'.
    Click it.
4.  LOOT will now download any updates to its master list file and
    compare the files it sees to this master list file. LOOT will
    display warning messages about the ITMs and deleted references the 3
    Bethesda ESMs contain. Do not worry about them at this time. They
    will be dealt with later, in the [Clean the Bethesda
    ESM\'s]{style="color:Gold"} step.
5.  The calculated load order is then displayed. Click
    [\'Apply\']{style="color:Gold"} to accept this order. If no changes
    are necessary there may not be an \'Apply\' option as no changes are
    needed.
6.  Close Loot using the Red X.

This process has loaded LOOT via Mod Organizer. This enables LOOT to act
upon any and all of the .ESM and .ESP files in Mod Organizer\'s virtual
file system. It compares what files it sees to the Master-List of all
.ESM and .ESP files and suggests tweaks as required. Some files need to
load before others or after others for their effects to be fully
noticeable. LOOT helps this to happen. If you look at the Right pane of
Mod Organizer now, you should notice that the files are in the same
order that you just sorted them into on the Left pane, with Skyrim.esm
and Update.esm above them. This is the correct formation.

This process has enabled the *Dawnguard, Hearthfires,* and *Dragonborn*
DLCs for use in the game.

Congratulations Mod Organizer 2 Base Profile should now be properly set
up. Next, we need to set up a Modding Profile.

### [ Creating Legacy Of The Dragonborn: Special Edition Profile ]{#Creating_Legacy_Of_The_Dragonborn:_Special_Edition_Profile .mw-headline}

We\'ve made a Vanilla Skyrim profile. This means you can play vanilla
unmodded Skyrim via Mod Organizer at a single mouse click. Now we\'ll
make the profile with which we\'ll mod Skyrim. This is how we proceed:

    * Click the [Configure Profiles] icon at the top of the main Mod Organizer window. The icon looks like an id card.
    * Select "Vanilla Skyrim SE" then click [Copy].
    * Type "Legacy Of The Dragonborn - Special Edition" in the text box and hit [OK].

A couple of points to be made here: Firstly, when you make a new
profile, Mod Organizer makes new copies of Skyrim.ini and
SkyrimPrefs.ini. If and when you make ini files changes, you will be
making them to THESE files. Secondly, we installed the base three files
of Skyrim Script Extender (SKSE) earlier, but didn\'t install the script
files. We will do this now to show how to install a mod via Mod
Organizer. Ensure you have your newly made profile active and then
perform the following steps:

    * Click the small screwdriver and wrench icon adjacent to the "Profile" drop-down and select "Install Mod...".
    * Navigate to the downloaded SKSE archive and click [Open].
    * Replace the default text, if any, in the "Name" text box with "SKSE Scripts". This is the name that will be displayed in the left pane post-installation.
    * In the file tree, navigate to the "Data" folder inside the archive, right-click and select "Set Data Directory". Only the Scripts folder should be visible and checked.
    * Click [OK].

The mod will now be displayed with an empty check box in the mod window.
This mod is now installed but not yet enabled for use by the game. Also,
note that Mod Organizer does not have a version number or category set
for this mod. To resolve this, perform the following:

    * Right-click the mod and select "Information...". (Alternatively, double click the mod name.)
    * In the "Categories" tab, check the box next to "Utilities" to add that category to this mod.
    * In the "Nexus Info" tab, set the version number to the installed SKSE version number but set the Mod ID to -1.

This has set the proper version number and category for this mod. The
colour of the text also indicates that SKSE is not available on the
Nexus. Look at the [\[File Tree\]]{style="color:Gold"} tab to see the
contents of the folder that will be virtually placed inside the
Skyrim/Data folder. Note the meta.ini file is shown which Mod Organizer
uses to store all the related data for the mod and it will not be
visible to the game when executed.

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[Any
mods downloaded directly into Mod Organizer from the Nexus will have one
or more of the data fields automatically filled based on Nexus
server-provided information.]{style="margin-left: 1em;color:#cacaca;"}
:::

    * Click [Close] to save and exit the window.

Mods version numbers that can be verified by the Nexus will be either
Green(Current+) or Red(Out Of Date). To make the mod visible to the
game:

    * Check the box adjacent to the mod name.

The files located inside this mod folder will now be visible to any
programs executed through Mod Organizer as if they were placed inside of
the Skyrim/Data folder. Any conflicts are handled by Mod Organizer in
priority order (higher priority overwrites lower priority). Give this a
try now:

    * Select "SKSE" in the drop down above the right pane then click [Run].

This will execute the game through Mod Organizer using the
SKSE\_Loader.exe. The Mod Organizer window will close as SKSE is
automatically configured to do so. At this point, SKSE is now properly
installed with all included script functionality. There is also a way to
check you have SKSE loaded and working:

    * When the game loads to the menu, open the console. This is usually one by pressing the key directly above the [Tab] key. It's often the ` or ~ key. This will open a small command prompt in the lower left corner. It may be hard to see but it should be there. Type the command "getskseversion" without the quotes. It should give output as to the version you have installed. If it's anything other than 2.0.8 then recheck the original SKSE install and the scripts install just performed.

Exit the game, and if needed ensure SKSE is correct again after checking
install. Do not continue until you\'re sure SKSE is correctly installed.

We\'ve now set up the profile we wish to install mods too. We\'ve also
set up a Vanilla Profile. Either a modded game or the vanilla game can
be played just by changing the profile. Additionally, should you wish to
make any other profile, you can simply copy that Vanilla Skyrim profile,
rename it and start with that new profile.

[ Clean the Bethesda ESM\'s ]{#Clean_the_Bethesda_ESM.27s .mw-headline}
-----------------------------------------------------------------------

With profiles now set up properly, it is time to begin modding some
vanilla game files. To increase stability, SSEEdit will be used to clean
ITMs and UDRs from Bethesda \'s ESMs. This improves compatibility
between the various DLCs and lessens the chance of instability in
Skyrim. Perform the following:

Before we start cleaning the ESMs we need to backup the originals,
navigate to Skyrim Special Edition\\Data folder and backup the
following:

    * Update.esm
    * Dawnguard.esm
    * HearthFires.esm
    * Dragonborn.esm

Once the original ESMs are backed up we can start the cleaning process:

    * Start Mod Organizer.
    * Select "SSEEdit" from the drop down above the right pane and click [Run].
    * Right-click inside the file listing and select "None".
    * Check the box adjacent to Update.esm and click [OK]. Wait for the message "Background Loader: finished" to appear in the right pane.
    * Right-click Update.esm and select "Apply Filter For Cleaning". Wait for the message "[Filtering Done]" to appear in the right pane.
    * Right-click Update.esm and select "Remove Identical To Master Records". Wait for the message "[Removing Identical to Master records done]".
    * Right-click Update.esm and select "Undelete and Disable References". Wait for the message "[Undeleting and Disabling References done]".
    * Click the [X] on the title bar to close SSEEdit.

Update.esm has now been cleaned, meaning that any improperly removed
records or edited records that were identical to the base Skyrim.esm
file have been fixed. **Repeat the process above for all of the DLC ESMs
in order selecting only one at a time. Do NOT attempt to clean
Skyrim.esm itself.**

Dawnguard.esm must also be cleaned twice to remove all duplicate
records. When you clean Dawnguard.esm the first time, check \"Backup
Plugins\". Do not check it the second time.

[Manual SSEEdit Cleaning For Dawnguard.esm Also
Required:]{style="color: gold"}

    Open SSEEdit and load only Dawnguard.esm
    Once loaded Left click and open CELL > Block 5 > Sub-Block 3 > select 00016BCF. Scroll down the view window on the right to find XEZN subrecord referring to RiftenRatwayZone [ECZN:0009FBB9]. Right click and select Remove.
    Next Left click and open CELL > Block 2 > Sub-Block 1 > select 0001FA4C. Right click and select Remove.
    Last Left click and open CELL > Block 8 > Sub-Block 1. Right click and select Remove.
    Click the [X] in the upper right hand corner, uncheck "Backup plugins", and click [OK].

Once you have finished the cleaning process Navigate to Mod Oraganizer
2/mods folder and create new folder called **Cleaned Vanilla ESMs**

Navitgate back to your Skyrim Special Edition **DATA** folder and Cut
and Paste these files:

-   Update.esm
-   Dawnguard.esm
-   Hearthfires.esm
-   Dragonborn.esm

Into the newly created **Cleaned Vanilla ESMs** Inside MO2/Mods folder.

Once you have cleaned and created a mod from the **Cleaned ESM\' *you
can restore the original*** *UNCLEANED* ones backed earlier to the
**DATA** folder.

This preserves an absolutely clean Skyrim/Data directory and also allows
our modded profile to run with the modified, cleaned vanilla ESM files.
To finish up the cleaning, perform the following:

    * Start Mod Organizer.
    * Verify the "Legacy Of The Dragonborn - Special Edition" profile is the currently selected.
    * Drag the newly created mod "Cleaned Vanilla ESMs" Above the "SKSE Scripts" mods in the left pane and activate it by checking the box.

To recap the current set up:

    * Running Skyrim through Steam results in a completely vanilla experience.
    * Running the "Vanilla Skyrim" profile through Mod Organizer with "Skyrim" selected in the drop-down results in a completely vanilla experience.
    * Running the "Legacy Of The Dragonborn - Special Edition" profile through Mod Organizer with "SKSE" selected in the drop-down results in a SKSE enabled experience using the cleaned base ESM files. 

So, now to start your modified game, always start Mod Organizer, select
SKSE and press \[RUN\].

[BethINI]{#BethINI .mw-headline}
--------------------------------

#### [[\[BethINI\]]{style="color:cyan"}]{#.5BBethINI.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[BethINI](https://www.nexusmods.com/skyrim/mods/69787/?){.external
.text} - 2.7.1 - by DoubleYou

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *BethINI*
:::
:::

Mod notes: This program was created by DoubleYou (The S.T.E.P. resident
INI Master). BethINI (pronounced \"Bethany\") is an attempt to restore
decency to the INI configuration files for games created by Bethesda.

Recommended Setting:

1.  Run BethINI.exe **[Outside of MO and make sure it is closed
    otherwise changes wont take effect]{style="color:Gold"}**
2.  Mod Organizer users should go to the Setup tab within BethINI and
    select the profile they wish to use via the *INI Path* setting. Your
    profiles should be autodetected.
3.  Under the Basic tab ***Click*** the **[DEFAULT
    BUTTON]{style="color:Gold"}**. This will refresh your INI files to
    the default settings for your system, with minimal tweaks.
4.  Under the Basic tab ***Click*** the **[HIGH
    PRESET]{style="color:Gold"}**.
5.  Under the Basic tab ***Make sure*** the **[BethINI
    PRESET]{style="color:Gold"}** is checked.
6.  Under the Basic tab ***Make sure*** the **[Recommended
    Tweaks]{style="color:Gold"}** is checked.
7.  Under the Basic Tab make sure ***Windowed Mode*** is
    **[UNCHECKED]{style="color:Gold"}**
8.  Under the Basic Tab make sure ***Borderless*** is
    **[UNCHECKED]{style="color:Gold"}**
9.  Under the Basic Tab make sure ***FXAA*** is
    **[CHECKED]{style="color:Gold"}**
10. Under the Basic tab make sure ***Antialiasing*** is set to
    **[NONE]{style="color:Gold"}**.
11. Under the Basic tab make sure ***VSync*** is
    **[UNCHECKED]{style="color:Gold"}**.
12. Under the Basic tab make sure ***Lock Frame Rate*** is
    **[UNCHECKED]{style="color:Gold"}**.
13. Under the General tab make sure ***Intro Logos*** is
    **[UNCHECKED]{style="color:Gold"}**.
14. Under the General Tab Make sure ***Post-Load Update Time*** is
    **[2000]{style="color:Gold"}**.
15. Under the Gameplay Tab Make Sure **Over-Encumbered Reminder** is set
    to **[60]{style="color:Gold"}**.
16. Under the Gameplay Tab Make Sure **NPC Use Ammo** is
    **[CHECKED]{style="color:Gold"}**.
17. Under the Gameplay Tab Make sure ***1st Person Arrow Tilt-up
    Angle*** is **[0.7]{style="color:Gold"}**.
18. Under the Gameplay Tab Make sure ***3rd Person Arrow Tilt-up
    Angle*** is **[0.7]{style="color:Gold"}**.
19. Under the Gameplay Tab Make sure ***1st Person Bolt Tilt-up Angle***
    is **[0.7]{style="color:Gold"}**.
20. Under the Interface Tab Make sure ***Mouse Settings -\> Lock
    Sensitivity*** is **[0.0125]{style="color:Gold"}**.
21. Under the Detail Tab make sure ***Field of View*** is set to
    **[85.00]{style="color:Gold"}**.
22. Under the Detail Tab make sure ***Particles*** is set to
    **[2000]{style="color:Gold"}**.
23. Under the Detail Tab make sure ***Depth of Field*** is
    **[CHECKED]{style="color:Gold"}**.
24. Under the Detail Tab make sure ***Lens Flare*** is
    **[UNCHECKED]{style="color:Gold"}**.
25. Under the Detail Tab make sure ***Reflect Sky*** is
    **[CHECKED]{style="color:Gold"}**.
26. Under the Detail Tab make sure ***Ambient Occlusion*** is set
    to**[NONE]{style="color:Gold"}**.
27. Under the Detail Tab make sure ***Shadow Resolution*** is set to
    **[2048]{style="color:Gold"}**.
28. Under the Detail Tab make sure ***Sun-Shadow Update Time*** is set
    to **[0]{style="color:Gold"}**.
29. Under the Detail Tab make sure ***Sun-Shadow Update Threshold*** is
    set to **[2.0]{style="color:Gold"}.**
30. Under the Detail Tab make sure ***Improved Shader*** is
    **[UNCHECKED]{style="color:Gold"}**.
31. Under the View Distance Tab make sure ***Grass Fade*** is set to
    **[18000]{style="color:Gold"}.**
32. Under the Visuals Tab make sure ***Grass Density*** is set to
    **[60]{style="color:Gold"}.**
33. Under the Visuals Tab make sure ***Grass Diversity*** is set to
    **[15]{style="color:Gold"}.**
34. Under the Basic Tab ***Click the* [Save and Exit
    button]{style="color:Gold"}.**

[Post BethINI Tweaks]{#Post_BethINI_Tweaks .mw-headline}
--------------------------------------------------------

##### [ [SKSE.ini]{style="color:cyan"} ]{#SKSE.ini .mw-headline}

The first step to editing the SKSE.ini is to create it. Perform the
following:

    * Right-click Skyrim Script Extender in the Mod Organizer mod list.
    * Select "Open In Explorer".
    * Create and enter a folder named SKSE.
    * Create and open a text file named SKSE.ini. (Make sure you are showing hidden file extensions)

Once this file is open to be edited, paste in the following:

    [General]
    ClearInvalidRegistrations=1 
    [Display]
    iTintTextureResolution=2048

This enables self-healing of rogue updating scripts in saves.

Save and close SKSE.ini then exit explorer.

##### [ [Skyrim.ini]{style="color:Cyan"} ]{#Skyrim.ini .mw-headline}

\[Grass\]

    bGrassPointLighting=1
    iMaxGrassTypesPerTexure=15 (make sure you have this.) 
    iMinGrassSize=60 (make sure you have this.) 

##### [ [SkyrimPrefs.ini ]{style="color:Cyan"} ]{#SkyrimPrefs.ini .mw-headline}

\[Controls\]

    bMouseAcceleration=0

\[Display\]

    bSAOEnable=0
    fInteriorShadowDistance=3000 (Make sure is this the value)

\[Grass\]

    b30GrassVS=0
    fGrassMaxStartFadeDistance=18000 (Make sure is this the value)

\[Main\]

    bSaveOnPause=0 (Change existing value)
    bSaveOnRest=0 (Change existing value)
    bSaveOnTravel=0 (Change existing value)
    bSaveOnWait=0 (Change existing value)

Due to the heavily modded nature of this guide, it is advised to have
autosaves turned off and to create fresh new saves every time and not
overwrite old preexisting saves.

[xLODGEN]{#xLODGEN .mw-headline}
--------------------------------

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[**Make
sure to Install xLODGEN Outside the main Skyrim SE Directory and the Mod
Organizer 2 mods folder**]{style="margin-left: 1em;color:#cacaca;"}
:::

#### [ [\[xLODGEN\]]{style="color:Cyan"} ]{#.5BxLODGEN.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[xLODGEN](https://mega.nz/#!kBoBnAbB!OKVh-gMBKCwKiKeDNMEQYgpQfyrH2vW9XMo119gjnZo){.external
.text} - v Beta 28 - by Sheson

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *xLODGen-28*
-   [**OPTIONAL FILES**]{style="color: #d0eb50"} -
    *[SSE-Terrain-Tamriel.esm](https://mega.nz/#!RVxiWBYA!Uipp7y8vwpPvyRnNRyHoLVT3wFjIEbIdmouMqfsQm60){.external
    .text}*
:::
:::

Special Installation:

1.  Download and install the Optional SSE-Terrain-Tamriel.esm file as a
    mod in Mod Organizer 2.
2.  Download the Main xLODGen-28 archive.
3.  Extract the contents to a new folder called
    [xLODGEN]{style="color:Gold"} but DO NOT PUT THE FOLDER INSIDE the
    main Skyrim SE OR Mod Organizer 2 folder.
4.  Inside the Newly created xLODGEN folder create another folder called
    [SSELODGen\_Output]{style="color:Gold"}
5.  Once the above is done, open Mod Organizer 2 and add xLODGenx64.exe
    as executables.
6.  In the [Arguments]{style="color:Gold"} field enter [-sse -o:\"\<Path
    where you installed xLODGEN\>\\SSELODGen\_Output\"
    ]{style="color:Gold"}.

<!-- -->

    for example my path is "E:\Skyrim Special Edition Tools\" and so in arguments: -sse -o:"E:\Skyrim Special Edition Tools\SSELODGen_Output"

Mod Notes: This is a utility used to generate LOD meshes and textures
\"exactly\" like CK.

[DynDOLOD]{#DynDOLOD .mw-headline}
----------------------------------

#### [ [\[DynDOLOD Standalone\]]{style="color:Cyan"} ]{#.5BDynDOLOD_Standalone.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[DynDOLOD
Standalone](https://mega.nz/#!AM5DTRQD!7GhAs6hCrOJjp4lw4RTAsl5Tqxuc_x54s2mf9gaBMlQ){.external
.text} - v2.44 - by Sheson

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} -
    *DynDOLOD-Standalone.2.44*
:::
:::

Special Installation:

1.  Download the archive file.
2.  Extract the contents to a new folder called [DynDOLOD Special
    Edition]{style="color:Gold"}
3.  Once the above is done, open Mod Organizer 2 and add TexGenx64 and
    DynDOLODx64 as executables.
4.  In the [Arguments]{style="color:Gold"} field enter
    [-sse]{style="color:Gold"} for each executable.

Mod Notes: This is a utility used to create a mod based on the load
order, which adds custom, distant LOD for objects to Skyrim.

[ Merge Plugins Standalone - Lexy\'s Legacy of The Dragonborn Special Edition Profile ]{#Merge_Plugins_Standalone_-_Lexy.27s_Legacy_of_The_Dragonborn_Special_Edition_Profile .mw-headline}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

[Idiot Check:]{style="color:Orange"} Make sure you have extracted the
Creation Kits scripts archive to your Data folder.

In this section we will be creating a new profile in Merge plugins
standalone so we can create new merged mods separate from SRLE base.

1.  Open Merge Plugin Standalone.
2.  Click the big plus symbol [**Create New
    Profile.**]{style="color:Gold"}
3.  Name the new profile [Lexy\'s LOTD SE]{style="color:Gold"}
4.  Path: This is where SkyrimSE.exe is on your system so for me it\'s
    [(H:\\Steam\\steamapps\\common\\Skyrim Special
    Edition\\).]{style="color:Gold"}
5.  Select your new [Lexy\'s LOTD SE]{style="color:Gold"} profile
    (turning it [lightblue]{style="color:lightblue"}) and click the
    [OK]{style="color:Gold"} button.
6.  Once it is loaded you should be asked to setup the new profile. If
    not click the **Gear Icon** in the tool bar at the top of Merge
    Plugin Standalone.
7.  Setup as per SRLE Base Instructions which can be found
    [HERE](http://wiki.step-project.com/User:Neovalen/Skyrim_Revisited_-_Legendary_Edition/Mod_Merging){.external
    .text}.

Check this Pictures on how to setup Merge Plugin Standalone:
[Here](https://imgur.com/a/nDpxe){.external .text}

This [GP video](https://www.youtube.com/watch?v=0S6cpCwTezE){.external
.text} gives you a complete explanation of Merge Plugin Standalone.
It\'s long, but it\'s worth watching, and some parts can be easily
skipped via the video\'s navigation tabs.

[ENB Installation]{#ENB_Installation .mw-headline}
--------------------------------------------------

#### [[\[ENB Series\]]{style="color:cyan"}]{#.5BENB_Series.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

[ENB Series](http://enbdev.com/mod_tesskyrimse_v0352.htm){.external
.text} - v0.352 - by Boris Vorontsov

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *ENBSeries v0.352 for TES
    Skyrim SE*
:::
:::

Specail Installation:

1.  The Download link is down the very bottom of the page.
2.  When the file has downloaded, open the Wrapper Version folder
    enclosed in the archive and extract ONLY the files: [d3d11.dll,
    d3dcompiler\_46e.dll, and enblocal.ini]{style="color:Gold"}
3.  Place the [d3d11.dll, d3dcompiler\_46e.dll, and
    enblocal.ini]{style="color:Gold"} into the Main Skyrim Special
    Edition Directory.
4.  Once extracted open the enblocal.ini and change the following
    setting:

<!-- -->

    Under [ENGINE], set ForceVSync=false (If running VSync through Driver)

[ GPU Driver Settings ]{#GPU_Driver_Settings .mw-headline}
----------------------------------------------------------

::: {style="margin: 1em;margin-left:50px;width:80%;border:solid #6ea5f5;border-width: 1px 0px;padding: 2px 1em 4px 5em;"}
[[[![Info-Logo.png](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/20px-Info-Logo.png){width="20"
height="20"
srcset="/images/thumb/4/40/Info-Logo.png/30px-Info-Logo.png 1.5x, /images/thumb/4/40/Info-Logo.png/40px-Info-Logo.png 2x"}](http://wiki.step-project.com/File:Info-Logo.png){.image}]{style="position: absolute;"}      Notice:]{style="margin-left:-4em; color:#ddd;font-weight:bold;"}[These
settings will be system dependent. What works for me may not work for
you.]{style="margin-left: 1em;color:#cacaca;"}
:::

#### [ [\[NVIDIA Profile Inspector\]]{style="color:Cyan"} ]{#.5BNVIDIA_Profile_Inspector.5D .mw-headline}

::: {style="border-left:medium solid #14ff17; padding-left:3px;"}
::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#006666; background:-webkit-linear-gradient(#006666, #669999); background:-moz-linear-gradient(#006666, #669999); background:-o-linear-gradient(#006666, #669999); background:linear-gradient(#006666, #669999); text-align:center; display:inline-block; padding:1px;"}
[***CORE***]{style="font-size:12px; color:#A8C9A7; line-height:1em;"}
:::

::: {style="cursor:default; border-radius:2px; width:42px; height:16px; background-color:#111111; background:-webkit-linear-gradient(#111111, #999999); background:-moz-linear-gradient(#111111, #999999); background:-o-linear-gradient(#111111, #999999); background:linear-gradient(#111111, #999999); text-align:center; display:inline-block; padding:1px;"}
[***TOOL***]{style="font-size:12px; color:#CCCCCC; line-height:1em;"}
:::

[Nvidia
Inspector](http://www.guru3d.com/files-details/nvidia-profile-inspector-download.html){.external
.text} - 2.1.2.0 - by Orbmu2k

::: {style="margin-left: 10px;"}
**Files to download** :

-   [**MAIN FILES**]{style="color: #d0eb50"} - *NVIDIA Inspector*
:::
:::

Special Installation:

-   Download and install NVIDIA Inspector, which includes NVIDIA Profile
    Inspector as a separate executable.
-   Download a [pre-made
    Profile](https://www.dropbox.com/s/iwc7d4suyrtbv5j/Skyrim%20SE%20Nvidia%20Inspector%20Profile.nip?dl=0){.external
    .text} .
-   Load NVIDIA Inspector and start NVIDIA Profile Inspector. On the
    Profile Inspector\'s tool bar there should be an icon of a box with
    green arrow pointing down. Hovering over the icon should display
    **Import user defined profiles** .
-   Once the profile have been installed, use the search bar and
    navigate to **The Elder Scrolls V: Skyrim Special Edition**.
-   Then double check the following has been set in the installed
    Profile:

<!-- -->

    2 - Sync and Refresh
    * Frame Rate Limiter                                            58  (From the drop down menu select 58)
    * Maximum pre-rendered frames                                   1
    * Vertical Sync                                                 Force On  (My game seems more smooth with these setting this may not be the same for you, but remember one rule, you must have Vsync enable somewhere either through the game, your ENB or the driver.)
    3 - Antialiasing
    * Antialiasing - Mode                                           Enhance the application setting
    * Antialiasing - Setting                                        4x [4x Multisampling]
    4 - Testure FIltering
    * Anisotropic filtering mode                                    User - defined/Off
    * Anisotropic filtering setting                                 16x
    * Texture Filtering - Anisoptropic Sample Optimization          On
    * Texture Filtering - Negative LOD bias                         Clamp (Leave at clamp)
    * Texture Filtering - Quality                                   High Quality (You can lower if needed)
    * Texture Filtering - Trilinear optimization                    On (will be ignored if using high quality)

[Now we can go back to the main page and start the modding adventure of
Skyrim Special Edition. Start at the \"Mod Installation\"
heading.]{style="color:magenta"} -\> [Lexy\'s: Legacy of The Dragonborn
Special
Edition](http://wiki.step-project.com/User:DarkladyLexy/Lexys_LOTD_SE){.external
.text}
:::

::: {.printfooter}
Retrieved from
\"[](http://wiki.step-project.com/index.php?title=User:DarkladyLexy/Lexys_LOTD_SE_Prerequisites&oldid=108474)[]{#YANDEX_0} [ http ]{.highlight
.highlight_active}[](http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&tld=com&lang=en&la=1538867840&tm=1539358780&text=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&l10n=en&mime=html&sign=a3939e1c720e8bd554e882e6bfc4433e&keyno=0#YANDEX_1)://[]{#YANDEX_1}[](http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&tld=com&lang=en&la=1538867840&tm=1539358780&text=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&l10n=en&mime=html&sign=a3939e1c720e8bd554e882e6bfc4433e&keyno=0#YANDEX_0)[[ wiki ]{.highlight
.highlight_active}](http://wiki.step-project.com/index.php?title=User:DarkladyLexy/Lexys_LOTD_SE_Prerequisites&oldid=108474)[](http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&tld=com&lang=en&la=1538867840&tm=1539358780&text=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&l10n=en&mime=html&sign=a3939e1c720e8bd554e882e6bfc4433e&keyno=0#YANDEX_2).[]{#YANDEX_2}[](http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&tld=com&lang=en&la=1538867840&tm=1539358780&text=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&l10n=en&mime=html&sign=a3939e1c720e8bd554e882e6bfc4433e&keyno=0#YANDEX_1)[[ step-project ]{.highlight
.highlight_active}](http://wiki.step-project.com/index.php?title=User:DarkladyLexy/Lexys_LOTD_SE_Prerequisites&oldid=108474)[](http://hghltd.yandex.net/yandbtm?fmode=inject&url=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&tld=com&lang=en&la=1538867840&tm=1539358780&text=http%3A%2F%2Fwiki.step-project.com%2FUser%3ADarkladyLexy%2FLexys_LOTD_SE_Prerequisites&l10n=en&mime=html&sign=a3939e1c720e8bd554e882e6bfc4433e&keyno=0#YANDEX_3).com/index.php?title=User:DarkladyLexy/Lexys\_LOTD\_SE\_Prerequisites&oldid=108474\"
:::

::: {#catlinks .catlinks .catlinks-allhidden}
:::
:::
:::

::: {.topbar}
-   [Forum](http://forum.step-project.com/){.dir}
-   [STEP](http://www.nexusmods.com/skyrim/mods/11){.dir}
    -   [@ NEXUS](http://www.nexusmods.com/skyrim/mods/11)
    -   [@
        FACEBOOK](http://www.facebook.com/pages/STEP-Skyrim-Total-Enhancement-Project/175289219235392)

<!-- -->

-   [Wiki]{.dir}
    -   [[Main
        page](http://wiki.step-project.com/Main_Page "Visit the main page [z]")]{#n-mainpage-description}
    -   [[STEP
        2.10.0](http://wiki.step-project.com/STEP:Guide)]{#n-STEP-2.10.0}
    -   [[Community
        portal](http://wiki.step-project.com/Project:Community_portal "About the project, what you can do, where to find things")]{#n-portal}
    -   [[Recent
        changes](http://wiki.step-project.com/Special:RecentChanges "A list of recent changes in the wiki [r]")]{#n-recentchanges}
    -   [[Random
        page](http://wiki.step-project.com/Special:Random "Load a random page [x]")]{#n-randompage}
    -   [[Help](http://wiki.step-project.com/Help:Contents "The place to find out")]{#n-help}
    -   [[Contact Us](mailto:admin@step-project.com)]{#n-Contact-Us}
    -   [[Search](http://wiki.step-project.com/Special:Search)]{#n-search}
-   [[Log
    in](http://wiki.step-project.com/index.php?title=Special:UserLogin&returnto=User%3ADarkladyLexy%2FLexys+LOTD+SE+Prerequisites "You are encouraged to log in; however, it is not mandatory [o]"){.dir}]{#pt-login}
-   
:::

::: {.botbar}
::: {#bot-icon}
:::

-   This page was last modified on October 4, 2018, at 07:12.

::: {#p-search}
![Quick
Search](User%20DarkladyLexy_Lexys%20LOTD%20SE%20Prerequisites%20-%20S.T.E.P.%20Project%20Wiki-Dateien/search-icon-md.png)
:::
:::
